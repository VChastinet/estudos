import { Component, OnInit } from '@angular/core';
import { IdbService } from './idb-handler/idb.service';
import { ClasseTeste } from './classeTeste';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-idb-test',
  templateUrl: './idb-test.component.html',
  styleUrls: ['./idb-test.component.css']
})
export class IdbTestComponent implements OnInit {

  public form: FormGroup;
  public idbContent: any;

  constructor(private idbService: IdbService, private fb: FormBuilder) {
    this.form = fb.group({
      formControl1: ['', Validators.required],
      formControl2: ['', Validators.required],
      formControl3: ['', Validators.required],
      formControl4: ['', Validators.required],
      formControl5: [null, Validators.required],
      formControl6: [false],
      storage: ['ItemA']
    });
  }

  ngOnInit() {
    this.getOne(this.form.get('storage').value);
  }

  // chamando INDEXED-DB
  public getContentIdb() {
    this.idbService.get()
      .then(response => {
        this.idbContent = response;
        console.log('conteúdo do IDB: ', response);
      }).catch(erro => {
        console.warn(erro.message);
      });
  }

  public saveContentIdb(key) {
    if (this.form.valid) {
      this.idbService.save(this.form.value, key)
        .then(response => {
          console.log('salvo: ', response);
          this.idbContent = 'conteúdo salvo no IDB';
        })
        .catch(erro => {
          console.warn(erro.message);
          alert(erro.message);
        });
    } else {
      alert('preencha o formulário inteiro');
    }
  }

  public change(key) {
    if (this.form.valid) {
      this.idbService.modify(this.form.value, key)
        .then(response => {
          console.log('alterado: ', response);
          this.idbContent = response;
        });
    } else {
      alert('preencha o formulário inteiro');
    }
  }

  public getOne(key) {
    this.idbService.getOne(key)
      .then(response => {
        console.log(key + ': ', response);
        this.idbContent = response;
        this.setFormValue(response);
      })
      .catch(erro => {
        alert('tentativa de acessar o IDB inexistente');
        console.warn(erro.message);
      });
  }

  public clear() {
    this.idbService.clear()
      .then(response => {
        this.form.reset();
        this.idbContent = 'IDB vazio';
        console.log('IDB vazio: ', response);
      });
  }

  public deleteOne(key) {
    this.idbService.deleteOne(key)
      .then(response => {
        this.form.reset();
        this.idbContent = key + ': excluída';
        console.log(key + ': excluída - ', response);
      });
  }

  private setFormValue(value) {
    try {
      this.form.setValue(value);
    } catch (error) {
      alert('caso o IDB esteja vazio, ocorrerá um erro ao tentar povoar o formulário');
      console.info(error.message);
    }
  }
}
