export class ClasseTeste {
  public id;
  public value;
  public description;
  constructor () {
    this.id = Math.random();
    this.value = {
      primary: Math.random(),
      secondary: Math.random()
    };
    this.description = 'descrição da instancia ' + this.id;
  }
}
