import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdbTestComponent } from './idb-test.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [IdbTestComponent],
})
export class IdbTestModule { }
