import { Injectable } from '@angular/core';
import { ConnectionFactory } from './idb-handler';
import { Dao } from './dao';

@Injectable({
  providedIn: 'root'
})

export class IdbService {

  public get() {
    return ConnectionFactory.getConnection('voxStorage')
      .then(connection => new Dao(connection, 'voxStorage'))
      .then(dao => dao.show());
  }

  public getOne(key) {
    return ConnectionFactory.getConnection('voxStorage')
      .then(connection => new Dao(connection, 'voxStorage'))
      .then(dao => dao.showByKey(key));
  }

  public save(content, key) {
    return ConnectionFactory.getConnection('voxStorage')
      .then(connection => new Dao(connection, 'voxStorage'))
      .then(dao => dao.adiciona(content, key));
  }

  public modify(content, key) {
    return ConnectionFactory.getConnection('voxStorage')
      .then(connection => new Dao(connection, 'voxStorage'))
      .then(dao => dao.altera(content, key));
  }

  public clear() {
    return ConnectionFactory.getConnection('voxStorage')
      .then(connection => new Dao(connection, 'voxStorage'))
      .then(dao => dao.clear());
  }

  public deleteOne(key) {
    return ConnectionFactory.getConnection('voxStorage')
      .then(connection => new Dao(connection, 'voxStorage'))
      .then(dao => dao.deleteByKey(key));
  }
}
