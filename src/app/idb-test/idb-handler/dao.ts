export class Dao {

  constructor(
    private connection: IDBDatabase,
    private store: string) {
  }

  adiciona(data, key): Promise<Event> {
    return new Promise((resolve, reject) => {
      const request = this.request().add(data, key);

      request.onsuccess = e => {
        resolve(e);
      };

      request.onerror = e => {
        console.warn(e.target['error']);
        reject(e.target['error']);
      };
    });
  }

  altera(data, key): Promise<Event> {
    return new Promise((resolve, reject) => {
      const request = this.request().openCursor(key);

      request.onsuccess = e => {
        const cursor: IDBCursor = e.target['result'];
        if (cursor) {
          cursor.update(data);
        }
        resolve(data);
      };

      request.onerror = e => {
        console.warn(e.target['error']);
        reject(e.target['error']);
      };
    });
  }

  show() {
    return new Promise((resolve, reject) => {
      const openCursor = this.request().openCursor();

      const content = [];

      openCursor.onsuccess = e => {
        const cursor: IDBCursor = e.target['result'];
        if (cursor) {
          const data = cursor['value'];
          if (data) {
            content.push(data);
          }
          cursor.continue();
          return;
        }
        resolve(content);
      };
      openCursor.onerror = e => {
        console.warn(e.target['error']);
        reject(e.target['error']);
      };
    });
  }

  showByKey(key) {
    return new Promise((resolve, reject) => {
      const openCursor = this.request().openCursor(key);

      openCursor.onsuccess = e => {
        const current = e.target['result'];
        if (current) {
          resolve(current.value);
        }
      };

      openCursor.onerror = e => {
        console.warn(e.target['error']);
        reject(e.target['error']);
      };
    });
  }

  clear() {
    return new Promise((resolve, reject) => {
      const request = this.request().clear();

      request.onsuccess = e => resolve();

      request.onerror = e => {
        console.warn(e.target['error']);
        reject(e.target['error']);
      };
    });
  }

  deleteByKey(key) {
    return new Promise((resolve, reject) => {
      const request = this.request().delete(key);

      request.onsuccess = e => resolve();

      request.onerror = e => {
        console.warn(e.target['error']);
        reject(e.target['error']);
      };
    });
  }

  private request() {
    return this.connection
      .transaction([this.store], 'readwrite')
      .objectStore(this.store);
  }
}
