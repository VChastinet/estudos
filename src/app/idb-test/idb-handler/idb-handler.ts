  export class ConnectionFactory {
    static stores = ['voxStorage'];
    static version = 1;
    static dbName = 'voxDatabase';
    static connection: IDBDatabase;

    constructor() {
      throw new Error('impossível criar instâncias desta classe');
    }

    static createStores(connection: IDBDatabase, stores: string[]) {
      stores.forEach(store => {
        if (connection.objectStoreNames.contains(store)) {
          return connection.deleteObjectStore;
        }
        connection.createObjectStore(store);
      });
    }

    static getConnection(...stores): Promise<IDBDatabase> {
      return new Promise((resolve, reject) => {
        const openRequest = window.indexedDB.open(ConnectionFactory.dbName, ConnectionFactory.version);

        openRequest.onupgradeneeded = e => {
          ConnectionFactory.createStores(e.target['result'], stores);
        };

        if (!!this.connection && !!stores.length) {
          ConnectionFactory.createStores(this.connection, stores);
        }

        openRequest.onsuccess = (event) => {
          if (!this.connection) {
            this.connection = event.target['result'];
          }

          resolve(this.connection);
        };

        openRequest.onerror = e => {
          reject(e.target['error'].name);
        };
      });
    }
  }

