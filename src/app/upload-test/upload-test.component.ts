import { Component, OnInit } from '@angular/core';
import { AnexosConfig } from 'projects/vox-upload/src/public_api';
import { TransitiveCompileNgModuleMetadata } from '@angular/compiler';

@Component({
  selector: 'app-upload-test',
  templateUrl: './upload-test.component.html',
  styleUrls: ['./upload-test.component.css']
})
export class UploadTestComponent implements OnInit {
  public arquivosConfig = <AnexosConfig> {
    apiUrl: 'https://www.mocky.io/v2/5185415ba171ea3a00704eed',
    listaAnexos: [
      {
        detalhe: 'anexo exemplo',
        hasAnexo: false,
        id: 12,
        requerido: false,
        // formato: ['pdf']
      },
      {
        detalhe: 'anexo exemplo obrigatorio enviado',
        hasAnexo: true,
        id: 22,
        requerido: true,
        // formato: ['pdf']
      },
      {
        detalhe: 'anexo exemplo obrigatorio',
        hasAnexo: false,
        id: 23,
        requerido: TransitiveCompileNgModuleMetadata,
        // formato: ['jpg', 'png']
      },
      {
        detalhe: 'anexo exemplo enviado',
        hasAnexo: true,
        id: 24,
        requerido: false,
        // formato: ['rar', 'zip']
      }
    ],
    formato: ['pdf', 'exe', 'rar']
  };
  constructor() { }

  ngOnInit() {
  }

}
