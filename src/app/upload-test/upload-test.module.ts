import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadTestComponent } from './upload-test.component';
import { UploadArquivosModule, CardUploadModule, ModalUploadModule } from 'projects/vox-upload/src/public_api';

@NgModule({
  imports: [
    CommonModule,
    UploadArquivosModule,
    CardUploadModule,
    ModalUploadModule
  ],
  declarations: [UploadTestComponent]
})
export class UploadTestModule { }
