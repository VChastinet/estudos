import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  template: `
    <div class="text-center shadow-sm p-3 mb-5 bg-white rounded">
      <h1 class="title">Selecione uma categoria</h1>

      <div class="card m-auto" style="width: 10rem;">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a [routerLink]="['idb']">IDB</a></li>
          <li class="list-group-item"><a [routerLink]="['diretiva']">Diretiva</a></li>
          <li class="list-group-item"><a [routerLink]="['upload']">Upload</a></li>
          <li class="list-group-item"><a [routerLink]="['response']">Response</a></li>
        </ul>
      </div>
    </div>
  `
})
export class HomepageComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
