import { Component, OnInit } from '@angular/core';
import { MockService } from './mock.service';

@Component({
  selector: 'app-response-test',
  templateUrl: './response-test.component.html',
  styleUrls: ['./response-test.component.css']
})
export class ResponseTestComponent {
  public result: any;
  constructor(private service: MockService) {}

  public callTest(status) {
    this.service.testing(status).subscribe(
      res => {
        alert('funfou');
        this.result = res;
        console.log('success call: ', res);
      },
      (err: Error) => {
        alert(err.message);
        console.log('error call: ', err.message);
      },
      () => console.log('completed')
    );
  }

  // callLoginError() {
  //   this.service.login().subscribe(
  //     res => {
  //       alert('funfou');
  //       this.result = res;
  //       console.log('success call: ', res);
  //     },
  //     (err: Error) => {
  //       alert(err.message);
  //       console.log('error call: ', err.message);
  //     },
  //     () => console.log('completed')
  //   );
  // }
}
