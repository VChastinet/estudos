import { serializable } from 'serializr';

export class Mocked {
  @serializable
  private result;
}
