import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { of, throwError } from 'rxjs';
import { deserialize, ClazzOrModelSchema } from 'serializr';

import { ResponseOptions } from './response-options.interface';

export class ResponseHandler {
  private requestResult: any;

  // substituir pelo cache do browser
  private cached: {};

  constructor() {
    this.cached = {};
    this.requestResult = {
      200: (options) => this.successResponse(options),
      202: (options) => this.successResponse(options, 'Requisição aceita'),
      203: (options) => this.successResponse(options, 'Acesso não permitido'),
      204: (options) => this.emptyResponse(),
      304: (response, key) => this.useCache(key),
      401: (response, key) => this.errorResponse(response[key], 'Não autorizado'),
      403: (response, key) => this.errorResponse(response[key], 'Proibido'),
      404: (response, key) => this.errorResponse(response[key], 'Não encontrado'),
      500: (response, key) => this.errorResponse(response[key], 'Erro no sistema')
    };
  }

  public treatError(error: { [key: string]: HttpErrorResponse }) {
    const key = Object.keys(error)[0];
    const statusCode = error[key].status;

    try {
      return this.requestResult[statusCode](error, key);
    } catch (e) {
      return this.errorResponse(error[key], 'Ocorreu um erro no requisição');
    }
  }

  public treatSuccess(response: { [key: string]: HttpResponse<any> }, modelSchema?: ClazzOrModelSchema<any>) {
    const key = Object.keys(response)[0];
    const statusCode = response[key].status;

    return this.requestResult[statusCode]({ success: response, key: key, model: modelSchema });
  }

  /**
  * @todo adicionar método para tratar erro do login
  */

  private errorResponse(error, defaultMsg) {
    const message = error.error['mensagem'] || defaultMsg;

    return throwError(Error(message));
  }

  private successResponse(options: ResponseOptions, message?: string) {
    const key = options.key;
    const response = options.success[key];

    if (!!message) {
      return response.body || message;
    }

    this.cached[key] = response.status === 200 && response;
    const result = options.model ? deserialize(options.model, response.body) : response.body;

    return result;
  }

  private emptyResponse() {
    return { noContent: true };
  }

  private useCache(key) {
    const cachedResponse = this.cached[key] || new HttpResponse({ body: { noContent: true }, status: 204 });
    return of(cachedResponse);
  }
}
