import { HttpResponse } from '@angular/common/http';
import { ClazzOrModelSchema } from 'serializr';

export interface ResponseOptions {
  success: { [key: string]: HttpResponse<any> };
  key: string;
  model?: ClazzOrModelSchema<any>;
}
