import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { map, catchError } from 'rxjs/operators';
import { ResponseHandler } from './response-handler/response-handler';
import { Mocked } from './mock-model';

@Injectable({
  providedIn: 'root'
})
export class MockService {
  private responseHandler: ResponseHandler;

    constructor(private http: HttpClient) {
        this.responseHandler = new ResponseHandler();
    }

    public testing(statusCode: number) {
        const urlsByCode = {
            200: 'https://www.mocky.io/v2/5ba011c93000002c007b1473',
            202: 'https://www.mocky.io/v2/5bae660c2e00006c00bb4228',
            203: 'https://www.mocky.io/v2/5ba0ee273500005e005bbb51',
            204: 'https://www.mocky.io/v2/5bae669e2e00007600bb422d',
            304: 'https://www.mocky.io/v2/5b9f96ab3000008500e28e78',
            401: 'https://www.mocky.io/v2/5ba0f2e435000055005bbb80',
            403: 'https://www.mocky.io/v2/5ba0f30b3500005c005bbb82',
            404: 'https://www.mocky.io/v2/5ba0f3273500005a005bbb85',
            500: 'https://www.mocky.io/v2/5ba0121a30000076007b1478',
            418: 'https://www.mocky.io/v2/5ba15e443500006b005bbd5d'
        };

        /* a opção 'observer' dever ser 'response' para receber a resposta completa */
        return this.http.get(urlsByCode[statusCode], {observe: 'response'}).pipe(
            catchError((err: HttpErrorResponse) => { /* o catch error deve vir em primeiro lugar */
                console.log(err);
                const result = this.responseHandler.treatError({test: err});
                console.log('service error handler: ', result);
                return result;
            }),
            map((res: HttpResponse<any>) => { /* map agora é obrigatório para que a resposta seja tratada */
                const result = this.responseHandler.treatSuccess({test: res}, Mocked);
                console.log('service success handler: ', result);
                return result;
            })
        );
    }

    // public login() {
    //   return this.http.get('http://www.mocky.io/v2/5ba0f2e435000055005bbb80', {observe: 'response'}).pipe(
    //     catchError((err: HttpErrorResponse) => {
    //       console.log(err);
    //       const result = this.responseHandler.treatLoginError({loginTest: err});
    //       console.log('service error handler: ', result);
    //       return result;
    //     }),
    //     map((res: HttpResponse<any>) => {
    //         const result = this.responseHandler.treatSuccess({loginTest: res});
    //         console.log('service success handler: ', result);
    //         return result;
    //     })
    //   );
    // }
}
