import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResponseTestComponent } from './response-test.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ResponseTestComponent]
})
export class ResponseTestModule { }
