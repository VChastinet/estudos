import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ComponentAModule } from './directive-tests/component-a/component-a.module';
import { ComponentAComponent } from './directive-tests/component-a/component-a.component';
import { IdbTestComponent } from './idb-test/idb-test.component';
import { IdbTestModule } from './idb-test/idb-test.module';
import { UploadTestComponent } from './upload-test/upload-test.component';
import { UploadTestModule } from './upload-test/upload-test.module';
import { ResponseTestModule } from './response-test/response-test.module';
import { ResponseTestComponent } from './response-test/response-test.component';
import { HomepageComponent } from './homepage/homepage.component';

const routes = [
  {path: 'diretiva', component: ComponentAComponent},
  {path: 'idb', component: IdbTestComponent},
  {path: 'upload', component: UploadTestComponent},
  {path: 'response', component: ResponseTestComponent},
  {path: '', component: HomepageComponent, pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentAModule,
    IdbTestModule,
    UploadTestModule,
    ResponseTestModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
