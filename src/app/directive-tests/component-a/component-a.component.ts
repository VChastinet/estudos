import { Component, OnInit } from '@angular/core';
import { MensagensCustom } from '../mensagem-validacao/mensagens-custom';

@Component({
  selector: 'app-component-a',
  templateUrl: './component-a.component.html',
  styleUrls: ['./component-a.component.css']
})
export class ComponentAComponent implements OnInit {

  condition = false;
  texto;
  public form = {
    erro: {
      maxLength: true
    }
  };

  constructor(private mensagens: MensagensCustom) { }

  ngOnInit() {
    this.mensagens.set(
      {custom: 'mensagem customizada'}
    );
  }

  testSwitch() {
    if (this.condition) {
      this.texto = 'texto true';
       return;
    }
    this.texto = 'lalalal';
  }

}
