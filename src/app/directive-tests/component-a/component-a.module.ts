import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentAComponent } from './component-a.component';
import { MensagemValidacaoModule } from '../mensagem-validacao/mensagem-validacao.module';

@NgModule({
  imports: [
    CommonModule,
    MensagemValidacaoModule
  ],
  declarations: [
    ComponentAComponent,
  ]
})
export class ComponentAModule { }
