export const MENSAGENS = {
  required: 'Este campo é obrigatório.',
  pattern: 'Este contém formato inválido.',
  maxLength: 'Este campo deve ter no máximo %s caracteres.',
  minLength: 'Este campo deve ter no mínimo %s caracteres.',
  max: 'O valor deste campo deve ser de no máximo %s',
  minx: 'O valor deste campo deve ser de no mínimo %s',
  email: 'Formato do email deve seguir o padrão: "nome@exemplo.com.br"',
};
