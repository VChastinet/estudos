import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MensagemValidacaoComponent } from './mensagem-validacao.component';
import { MensagemValidacaoDirective } from './mensagem-validacao.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MensagemValidacaoComponent,
    MensagemValidacaoDirective
  ],
  entryComponents: [MensagemValidacaoComponent],
  exports: [MensagemValidacaoDirective]
})
export class MensagemValidacaoModule { }
