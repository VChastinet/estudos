import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MensagensCustom {
  private mensagens;
  constructor() {}

  public set(value: {[key: string]: string}) {
    this.mensagens = value;
  }

  public get(): {[key: string]: string} {
    return this.mensagens;
  }
}
