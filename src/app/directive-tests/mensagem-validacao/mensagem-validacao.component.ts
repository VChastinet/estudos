import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { MensagensCustom } from './mensagens-custom';
import { MENSAGENS } from './mensagens-padrao';

@Component({
  selector: 'app-mensagem-validacao',
  template: `
    <ng-template #template>
      <p class="text-danger" [hidden]="!show">{{ mensagemErro }}</p>
    </ng-template>
  `,
  styles: ['p { margin: 0; }']
})

export class MensagemValidacaoComponent {
  @ViewChild('template', { read: TemplateRef }) private _template;
  public show: boolean;
  private _tipoErro: { [key: string]: string };
  private _length: {min: number; max: number};

  constructor(private mensagens: MensagensCustom) { }

  @Input()
  public set tipoErro(value: { [key: string]: string }) {
    this._tipoErro = value;
  }

  @Input() public set length (value: {min: number; max: number}) {
    this._length = value;
  }

  public get template(): TemplateRef<MensagemValidacaoComponent> {
    return this._template;
  }

  public get mensagemErro() {
    Object.assign(MENSAGENS, this.mensagens.get());

    const primeiroErro = Object.keys(this._tipoErro)[0];
    let mensagem = MENSAGENS[primeiroErro];

    if (primeiroErro.match('max') && this._length.max) {
      mensagem = mensagem.replace('%s', this._length.max);
    }

    if (primeiroErro.match('min') && this._length.min) {
      mensagem = mensagem.replace('%s', this._length.min);
    }

    return mensagem;
  }
}
