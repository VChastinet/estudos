import { Directive, TemplateRef, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { MensagemValidacaoComponent } from './mensagem-validacao.component';

@Directive({
  selector: '[mensagemValidacao]'
})
export class MensagemValidacaoDirective {
  private componentInstance: MensagemValidacaoComponent;

  constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef, private resolver: ComponentFactoryResolver) {
    const factory = this.resolver.resolveComponentFactory(MensagemValidacaoComponent);
    const componentRef = this.viewContainer.createComponent(factory);
    this.componentInstance = componentRef.instance;

    this.viewContainer.createEmbeddedView(this.templateRef);
    this.viewContainer.createEmbeddedView(this.componentInstance.template);
  }

  @Input()
  public set mensagemValidacao(
    {show, erro, length = {min: 0, max: 0}}:
    {show: boolean; erro: {[key: string]: string}, length: {min: number; max: number}}
  ) {
    this.componentInstance.tipoErro = erro;
    this.componentInstance.show = show;
    this.componentInstance.length = length;
    show
      ? this.templateRef.elementRef.nativeElement.nextSibling.nextSibling.style.borderColor = 'red'
      : this.templateRef.elementRef.nativeElement.nextSibling.nextSibling.style.borderColor = null;
  }

}
