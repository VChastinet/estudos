/*
 * Public API Surface of vox-upload
 */

export * from './lib/upload-arquivos/upload-arquivos.module';
export * from './lib/upload-arquivos/upload-arquivos.index';
export * from './lib/card-upload/card-upload.module';
export * from './lib/modal-upload/modal-upload.module';
