import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { ResponseUtil } from '../response.util';
import { AnexosConfig } from '../upload-arquivos/upload-arquivos.index';
import { UploadService } from '../upload-arquivos/upload.service';

@Component({
    selector: 'lib-card-upload',
    templateUrl: './card-upload.component.html',
    styleUrls: ['./card-upload.component.css']
})
export class CardUploadComponent {
    @Input() public anexosConfig: AnexosConfig | any;
    @Output() public arquivoAnexado: EventEmitter<any>;
    @Output() public anexoCardOpen: EventEmitter<string>;

    constructor(config: NgbAccordionConfig, private uploadService: UploadService) {
        this.anexoCardOpen = new EventEmitter();
        this.arquivoAnexado = new EventEmitter();
        config.type = 'card';
    }

    /**
     * emite um evento caso o card seja aberto
     * @param {*} event
     * @memberof CardUploadComponent
     */
    onPanelChange(event): void {
        if (event.nextState) {
            this.anexoCardOpen.emit(event.panelId);
        }
    }

    public get erroInfo(): {statusClass: string, label: string} {
        return ResponseUtil.erroLabel(this.uploadService.tipoErro);
    }

    public tentarNovamente(): void {
        this.anexoCardOpen.emit();
    }

    public onArquivoAnexado(event) {
      this.arquivoAnexado.emit(event);
    }
}
