import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadingLocalModule } from '@voxtecnologia/vox-preload';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CardUploadComponent } from './card-upload.component';
import { UploadArquivosModule } from '../upload-arquivos/upload-arquivos.module';



@NgModule({
    imports: [
        CommonModule,
        LoadingLocalModule,
        NgbModule,
        UploadArquivosModule
    ],
    declarations: [CardUploadComponent],
    exports: [CardUploadComponent]
})
export class CardUploadModule {}
