import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AnexosConfig } from '../upload-arquivos/upload-arquivos.index';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UploadService } from '../upload-arquivos/upload.service';
import { ResponseUtil } from '../response.util';

@Component({
  selector: 'lib-modal-upload',
  templateUrl: './modal-upload.component.html',
  styleUrls: ['./modal-upload.component.css']
})
export class ModalUploadComponent implements OnInit {

  @Input() public anexosConfig: AnexosConfig | any;
  @Output() public arquivoAnexado: EventEmitter<any>;
  @Output() public modalOpen: EventEmitter<any>;
  public enviados: { listaAnexosEnviados: number[]; requeridosPendentes: boolean };
  private modalRef: NgbModalRef;

  constructor(private modalService: NgbModal, private uploadService: UploadService) {
    this.arquivoAnexado = new EventEmitter();
    this.modalOpen = new EventEmitter();
    this.enviados = {
      listaAnexosEnviados: [],
      requeridosPendentes: true
    };
  }

  ngOnInit() {
  }

  public open(anexar) {
    this.modalRef = this.modalService.open(anexar, {size: 'lg'});
    this.modalOpen.emit(true);
  }

  public get erroInfo(): {statusClass: string, label: string} {
    return ResponseUtil.erroLabel(this.uploadService.tipoErro);
  }
  public onArquivoAnexado(event) {
    console.log(
      event
    );

    this.arquivoAnexado.emit(event);
    this.enviados = event;
    this.modalRef.close();
  }

  public checaEnviado(solicitado): boolean {
    return this.enviados.listaAnexosEnviados.some(anexoId => anexoId === solicitado);
  }

  public desabilitaBotao(): boolean {
    return this.enviados.listaAnexosEnviados.length === this.anexosConfig.listaAnexos.length;
  }
}
