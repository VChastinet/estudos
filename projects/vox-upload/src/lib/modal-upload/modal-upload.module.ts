import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalUploadComponent } from './modal-upload.component';
import { UploadArquivosModule } from '../upload-arquivos/upload-arquivos.module';

@NgModule({
  imports: [
    CommonModule,
    UploadArquivosModule
  ],
  declarations: [ModalUploadComponent],
  exports: [ModalUploadComponent]
})
export class ModalUploadModule { }
