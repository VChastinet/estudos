export const ICONS = {
  'fa-file-image-o': ['jpg', 'jpeg', 'png'],
  'fa-file-pdf-o ': ['pdf'],
  'file-word-o': ['doc', 'odt'],
  'fa-file-excel-o': ['obs', 'xls'],
  'fa-file-text-o': ['txt'],
  'fa-file-archive-o': ['zip', 'rar']
};
