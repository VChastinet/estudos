export enum ExtensaoArquivoEnum {
    /**
     * arquivo do tipo: Joint Photographic Group
     */
    JPG = 'jpg',
    /**
     * arquivo do tipo: Joint Photographic Experts Group
     */
    JPEG = 'jpeg',
    /**
     * arquivo do tipo: Portable Network Graphics
     */
    PNG = 'png',
    /**
     * arquivo do tipo: Portable Document Format
     */
    PDF = 'pdf',
    /**
     * arquivo do tipo: Microsoft document File
     */
    DOC = 'doc',
    /**
     * arquivo do tipo: open document text
     */
    ODT = 'odt',
    /**
     * arquivo do tipo: open document spreadsheet
     */
    ODS = 'obs',
    /**
     * arquivo do tipo: Microsoft Excel spreadsheet file
     */
    XLS = 'xls',
    /**
     * arquivo do tipo: text
     */
    TXT = 'txt',

    /**
     * arquivo compactado do tipo zip
     */
    ZIP = 'zip',

    /**
     * arquivo compactado do tipo: Roshal Archive
     */
    RAR = 'rar'
}
