import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { AnexoSelecionado } from './interface/anexo-selecionado.model';
import { UploadArquivosModule } from './upload-arquivos.module';
import { catchError } from 'rxjs/operators';

/**
 * serviço que envia os anexos
 * @export
 * @class UploadService
 */
@Injectable()
export class UploadService {
  public tipoErro: 'vazio' | 'erro' | undefined;
    /**
     *Creates an instance of UploadService.
     * @param {HttpClient} http
     * @memberof UploadService
     */
    constructor(private http: HttpClient) {}

    /**
     * método post do serviço
     * @param {string} apiUrl
     * @param {*} dados
     * @returns {Observable<any>}
     * @memberof UploadService
     */
    public enviarAnexos(apiUrl: string, dados: AnexoSelecionado): Observable<any> {
        return this.http.post(apiUrl, this.preparaArquivo(dados), { withCredentials: true }).pipe(
          catchError(erro => throwError(new Error(erro.error['mensagem'] || 'Ocorreu um erro')))
        );
    }

    /**
     * monta o arquivo no formato adequado para o envio
     * @private
     * @param {*} dados
     * @returns
     * @memberof UploadService
     */
    private preparaArquivo(dados: AnexoSelecionado) {
        const formData = new FormData();
        formData.append('files', dados.arquivo);
        formData.append('tipo', String(dados.id));
        return formData;
    }
}
