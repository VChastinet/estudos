import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { LoadingLocalService, LoadingInputService } from '@voxtecnologia/vox-preload';
import { AnexosSolicitados } from './interface/anexos-solicitados.interface';
import { VoxAlertService } from '@voxtecnologia/alert';
import { AnexosConfig } from './interface/anexos-config.interface';
import { UploadService } from './upload.service';
import { AnexoSelecionado } from './interface/anexo-selecionado.model';
import { ResponseUtil } from '../response.util';
import { ICONS } from '../icons';

/**
 * card responsável com anexar e enviar arquivos
 * @export
 * @class UploadArquivosComponent
 * @implements {OnChanges}
 */
@Component({
    selector: 'lib-upload-arquivos',
    templateUrl: './upload-arquivos.component.html',
    styleUrls: ['./upload-arquivos.component.css']
})

export class UploadArquivosComponent implements OnChanges {
    public disableButtons: boolean;
    @Output() public arquivoAnexado: EventEmitter<any>;
    @Input() public anexosConfig: AnexosConfig | any;

    private _arquivosSelecionados: object;
    private listaAnexosSelecionados: AnexoSelecionado[];
    private listaAnexos: AnexosSolicitados[];
    private formatos: string[];
    private tamanho: number;
    @ViewChildren('inputRef') private fileInput: QueryList<ElementRef>;

    /**
     * Creates an instance of UploadArquivosComponent.
     * @param {LoadingLocalService} loadingLocal
     * @param {VoxAlertService} voxAlert
     * @memberof AnexosComponent
     */
    constructor(
        private loadingIput: LoadingInputService,
        private voxAlert: VoxAlertService,
        private uploadService: UploadService
    ) {
        this.arquivoAnexado = new EventEmitter();
        this._arquivosSelecionados = new Object();
        this.listaAnexosSelecionados = [];
    }

    /**
     * esconde o componente de loading no momento em que os dados são recebidos
     * seta os valores dos parâmetros de validação
     * @memberof AnexosComponent
     */
    public ngOnChanges(): void {
        if (!!this.anexosConfig) {
          ResponseUtil.manager(this.anexosConfig.listaAnexos)
                .then((res: AnexosSolicitados[]) => {
                    this.listaAnexos = res;
                    this.formatos = this.anexosConfig.formato || [];
                    this.tamanho = this.anexosConfig.tamanho || 0;
                    this.trataEscolhaFormatos(res);
                })
                .catch((err: 'erro' | 'vazio') => {
                    if (err !== 'erro' && err !== 'vazio') {
                      this.voxAlert.openModal(err['message'], 'Erro', 'danger');
                      console.error(err['message']);
                    }

                    this.uploadService.tipoErro = err;
                });
        }
    }

    /**
     * getter dos arquivos selecionados
     * @readonly
     * @memberof UploadArquivosComponent
     */
    public get arquivosSelecionados(): Object {
        return this._arquivosSelecionados;
    }

    /**
     * verifica se existe uma lista de anexos
     * @readonly
     * @type {boolean}
     * @memberof AnexosComponent
     */
    public get hasListaAnexos(): boolean {
        return this.listaAnexos && !!this.listaAnexos.length;
    }

    public get listaFormatos(): string {
      return this.formatos.length && this.formatos.join(' - ');
    }

    /**
     * verifica se um anexo específico foi adicionado
     * @param {*} anexoId
     * @returns {boolean}
     * @memberof UploadArquivosComponent
     */
    public hasAnexo(anexoId): boolean {
        return !!this._arquivosSelecionados[anexoId];
    }

    /**
     * checa se já existem arquivos selecionados
     * @returns {boolean}
     * @memberof UploadArquivosComponent
     */
    public hasAnexosSelecionados(): boolean {
        return !!this.listaAnexosSelecionados.length;
    }

    /**
     * checa se ja existem arquivos que foram enviados
     * @param {*} anexoId
     * @returns {boolean}
     * @memberof UploadArquivosComponent
     */
    public isAnexoEnviado(anexoId): boolean {
        return this._arquivosSelecionados[anexoId].hasAnexo;
    }

    /**
     * evento disparado caso um arquivo seja adicionado ou removido
     * @param {*} event
     * @param {*} anexoId
     * @memberof UploadArquivosComponent
     */
    public selecionarArquivo(event: any, anexo: AnexosSolicitados): void {
        const nome = event.target.files[0] && event.target.files[0].name;
        const arquivo = event.target.files[0];

        this.validaArquivo(arquivo, anexo.formato)
            .then(res => {
                this.adicionaArquivo(nome, arquivo, anexo);
            })
            .catch(erro => {
                this.voxAlert.openModal(erro.message, 'Atenção', 'warning');
                this.removeArquivo(anexo, event.target);
            });
    }

    /**
     * remove arquivo da lista
     * @param {AnexosSolicitados} anexo
     * @param {HTMLInputElement} inputRef
     * @memberof UploadArquivosComponent
     */
    public removeArquivo(anexo: AnexosSolicitados, inputRef: HTMLInputElement): void {
        inputRef.value = '';
        delete this._arquivosSelecionados[anexo.id];
        this.listaAnexosSelecionados = this.ObjectToArray(this._arquivosSelecionados);
    }

    /**
     * envia os anexos selecionados
     * @memberof UploadArquivosComponent
     */
    public enviar(): void {
        this.disableButtons = true;
        this.enviarAnexos()
            .then(res => {
                this.disableButtons = false;
                this.voxAlert.openModal('Arquivos enviados com sucesso', 'Sucesso', 'success');
                this.emiteListaAnexos();
                this.limpaListas();
            })
            .catch(error => {
                this.disableButtons = false;
                this.voxAlert.openModal(error, 'Erro', 'danger');
            });
    }

    public setIcons(tipoArquivo) {
      const icone = Object.keys(ICONS)
        .map(icon => ICONS[icon].some(file => file.toUpperCase() === tipoArquivo.toUpperCase()) && icon)
        .filter(item => !!item)[0];

      return icone;
    }

    /**
     * transforma um objeto em array
     * @private
     * @param {Object} object
     * @returns {any[]}
     * @memberof UploadArquivosComponent
     */
    private ObjectToArray(object: Object): any[] {
        return Object.keys(object).map(props => object[props]);
    }

    /**
     * adiciona o arquivo selecionado na lista
     * @private
     * @param {*} nome
     * @param {*} arquivo
     * @param {*} anexo
     * @memberof UploadArquivosComponent
     */
    private adicionaArquivo(nome: string, arquivo: File, anexo: AnexosSolicitados): void {
        const arquivos = new AnexoSelecionado(anexo.id, nome, arquivo, anexo.requerido);

        this._arquivosSelecionados[anexo.id] = arquivos;
        this.listaAnexosSelecionados = this.ObjectToArray(this._arquivosSelecionados);
    }

    /**
     * checa se todos os arquivos requeridos foram anexados
     * emite a lista de eventos assim que todos os arquivos requeridos forem anexados
     * @private
     * @memberof UploadArquivosComponent
     */
    private emiteListaAnexos(): void {
        const anexosSolicitados = this.listaAnexos.filter(anexo => anexo.requerido).length;
        const anexosAdicionados = this.listaAnexosSelecionados.filter(anexo => anexo.requerido).length;
        const anexosJaEnviados = this.listaAnexos.filter(anexo => anexo.requerido && anexo.hasAnexo).length;

        const anexosValidados = anexosSolicitados === anexosAdicionados || anexosSolicitados === anexosJaEnviados;

        if (anexosValidados) {
            this.arquivoAnexado.emit(this.anexosAEmitir(false));
            return;
        }
        this.arquivoAnexado.emit(this.anexosAEmitir(true));
    }

    private anexosAEmitir(requeridosPendentes) {
      return {
        requeridosPendentes: requeridosPendentes,
        listaAnexosEnviados: this.listaAnexos.filter(anexo => anexo.hasAnexo).map(anexo => anexo.id)
      };
    }

    /**
     * faz a validação do arquivo de acordo com os critérios
     * @private
     * @param {*} arquivoSelecionado
     * @returns
     * @memberof UploadArquivosComponent
     */
    private validaArquivo(arquivoSelecionado: File, formatoPorArquivo): Promise<void> {
        const tamanhoAtual = arquivoSelecionado.size;
        const tamanhoPermitido = this.tamanho;
        const formatosPermitidos = this.formatos;
        const formatoAtual = arquivoSelecionado.name.replace(/\.(\w+)$/, '$1').toUpperCase();
        const checaFormato = this.checaFormato(formatosPermitidos, formatoAtual, formatoPorArquivo);
        const arquivoRepetido = this.listaAnexosSelecionados.some(item => item.nome === arquivoSelecionado.name);

        return this.validaRepetido(arquivoRepetido)
            .then(res => this.validaFormato(formatosPermitidos, checaFormato))
            .then(res => this.validaTamanho(tamanhoPermitido, tamanhoAtual));
    }

    /**
     * checa se o arquivo selecionado já existe
     * @private
     * @param {boolean} arquivoRepetido
     * @memberof UploadArquivosComponent
     */
    private async validaRepetido(arquivoRepetido: boolean): Promise<void> {
        if (arquivoRepetido) {
            throw Error('Arquivo já selecionado');
        }
    }

    /**
     * checa se o formato do arquivo está na lista de permissão
     * @private
     * @param {string[]} formatosPermitidos
     * @param {boolean} checaFormato
     * @memberof UploadArquivosComponent
     */
    private async validaFormato(formatosPermitidos: string[], checaFormato: boolean): Promise<void> {
        if (formatosPermitidos.length && !checaFormato) {
            throw Error(`O arquivo selecionado deve ser dos seguintes formatos: ${formatosPermitidos.join(', ')}`);
        }
    }

    private checaFormato(formatosPermitidos, formatoAtual, formatoPorArquivo) {
      return formatoPorArquivo
        ? formatoPorArquivo.some(formato => !!formatoAtual.match(formato.toUpperCase()))
        : formatosPermitidos.some(formato => !!formatoAtual.match(formato.toUpperCase()));
    }

    /**
     * checa se o tamanho do arquivo é válido
     * @private
     * @param {number} tamanhoPermitido
     * @param {number} tamanhoAtual
     * @memberof UploadArquivosComponent
     */
    private async validaTamanho(tamanhoPermitido: number, tamanhoAtual: number): Promise<void> {
        if (tamanhoPermitido && !(tamanhoAtual <= tamanhoPermitido * 1000000)) {
            throw Error(`O arquivo selecionado deve ter no máximo ${tamanhoPermitido} mb`);
        }
    }

    /**
     * itera pelos arquivos selecionados e realiza o request para cada um deles
     * @private
     * @memberof UploadArquivosComponent
     */
    private async enviarAnexos(): Promise<void> {
        for (const anexo of this.listaAnexosSelecionados) {
            this.loadingIput.show(`nome-${anexo.id}`, 'Enviando');
            await this.anexarRequest(anexo);
        }
    }

    /**
     * request do serviço de anexar
     * @private
     * @param {AnexoSelecionado} anexo
     * @returns {Promise<object>}
     * @memberof UploadArquivosComponent
     */
    private anexarRequest(anexo: AnexoSelecionado): Promise<object> {
        return new Promise((resolve, reject) => {
            this.uploadService.enviarAnexos(this.anexosConfig.apiUrl, anexo).subscribe(
                response => {
                    this.loadingIput.hide(`nome-${anexo.id}`, 'success');
                    this.setHasAnexo(anexo);
                    resolve(response);
                },
                error => {
                    this.loadingIput.hide(`nome-${anexo.id}`, 'error', {error: 'Falha'});
                    reject(`Erro ao enviar o anexo "${anexo.nome}"`);
                }
            );
        });
    }

    /**
     * seta o valor da propriedade que indica se o arquivo foi enviado
     * @private
     * @param {AnexoSelecionado} anexo
     * @memberof UploadArquivosComponent
     */
    private setHasAnexo(anexo: AnexoSelecionado): void {
        this.listaAnexos = this.listaAnexos.map(item => {
            if (item.id === anexo.id) {
                item.hasAnexo = true;
            }
            return item;
        });
    }

    /**
     * limpa as variável quem guardam os anexos que foram enviados
     * @private
     * @memberof UploadArquivosComponent
     */
    private limpaListas(): void {
        this.listaAnexosSelecionados = [];
        this._arquivosSelecionados = {};
        this.fileInput.map(element => {
            element.nativeElement.value = '';
        });
    }

    private trataEscolhaFormatos(arquivo) {
      const hasListaGeral = this.listaFormatos.length;
      const hasListaPorArquivo = arquivo.filter(anexo => anexo.formato).length;

      if (hasListaGeral && hasListaPorArquivo) {
        throw new Error('só é possível um tipo de lista de formatos válidos (geral ou individual)');
      }

      if (!hasListaGeral && !hasListaPorArquivo) {
        throw new Error('escolha pelo menos um tipo de lista de formatos válidos (geral ou individual)');
      }
    }
}
