import { ExtensaoArquivoEnum } from '../extensao-arquivo.enum';

export interface AnexosSolicitados {
  detalhe: string;
  id: number;
  requerido: boolean;
  hasAnexo: boolean;
  formato?: ExtensaoArquivoEnum[];
}
