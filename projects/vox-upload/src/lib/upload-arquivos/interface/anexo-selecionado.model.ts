import { ExtensaoArquivoEnum } from '../extensao-arquivo.enum';

export class AnexoSelecionado {
  private _id: number;
  private _nome: string;
  private _arquivo: File;
  private _requerido: boolean;
  private _hasAnexo: boolean;
  private _formato: ExtensaoArquivoEnum[];

  constructor(id: number, nome: string, arquivo: File, requerido: boolean, formato?: ExtensaoArquivoEnum[]) {
    this._id = id;
    this._nome = nome;
    this._arquivo = arquivo;
    this._requerido = requerido;
    this._formato = formato;
  }

  /**
   * Getter id
   * @return {number}
   */
  public get id(): number {
    return this._id;
  }

  /**
   * Getter nome
   * @return {string}
   */
  public get nome(): string {
    return this._nome;
  }

  /**
   * Getter arquivo
   * @return {File}
   */
  public get arquivo(): File {
    return this._arquivo;
  }

  /**
   * Getter requerido
   * @return {boolean}
   */
  public get requerido(): boolean {
    return this._requerido;
  }

  /**
   * Getter hasAnexo
   * @return {boolean}
   */
  public get hasAnexo(): boolean {
    return this._hasAnexo;
  }

  /**
   * Setter hasAnexo
   * @type {void}
   * @memberof AnexoSelecionado
   */
  public set hasAnexo(value) {
    this._hasAnexo = value;
  }


  /**
   * Getter formato
   * @return {boolean}
   */
  public get formato(): ExtensaoArquivoEnum[] {
    return this._formato;
  }

  /**
   * Setter formato
   * @type {void}
   * @memberof AnexoSelecionado
   */
  public set formato(value: ExtensaoArquivoEnum[]) {
    this._formato = value;
  }
}
