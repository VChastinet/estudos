import { AnexosSolicitados } from './anexos-solicitados.interface';
import { ExtensaoArquivoEnum } from '../extensao-arquivo.enum';

export interface AnexosConfig {
    listaAnexos: AnexosSolicitados[];
    apiUrl: string;
    formato?: ExtensaoArquivoEnum[];
    tamanho?: number;
}
