import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VoxAlertModule,  } from '@voxtecnologia/alert';
import { LoadingLocalModule, LoadingInputModule } from '@voxtecnologia/vox-preload';
import { UploadArquivosComponent } from './upload-arquivos.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UploadService } from './upload.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LoadingLocalModule,
    LoadingInputModule,
    VoxAlertModule
  ],
  declarations: [UploadArquivosComponent],
  exports: [UploadArquivosComponent],
  providers: [UploadService]
})
export class UploadArquivosModule { }
